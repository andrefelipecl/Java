package br.com.afcl.interfaces.compose;

import br.com.afcl.entity.Veiculo;
import br.com.afcl.interfaces.CalculoValor;
import static br.com.afcl.constants.Constants.*;

public class CalculoDiaria implements CalculoValor {

	private double valorDiaria;
	
	
	public CalculoDiaria(double valorDiaria) {
		super();
		this.valorDiaria = valorDiaria;
	}

	@Override
	public Double calcular(double periodo, Veiculo veiculo) {
		return valorDiaria * Math.ceil(periodo / HORA);
	}
}
