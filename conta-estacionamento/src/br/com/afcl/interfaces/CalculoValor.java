package br.com.afcl.interfaces;

import br.com.afcl.entity.Veiculo;

public interface CalculoValor {

	/**
	 * M�todo que deve ser composto para calcular os diferentes tipos de tarifas.
	 * 
	 * @return
	 */
	public Double calcular(double periodo, Veiculo veiculo);
}
