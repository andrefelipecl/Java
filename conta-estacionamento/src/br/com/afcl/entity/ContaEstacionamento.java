package br.com.afcl.entity;

import br.com.afcl.interfaces.CalculoValor;

/**
 * DESCRIÇÃO:
 * 
 * Para um veÍculo de passeio, o valor deve ser calculado como R$2,00 por hora,
 * porém, caso o tempo seja maior do que 12 horas, será cobrada uma taxa di�ria,
 * e caso o n�mero de dias for maior que 15 dias, ser� cobrada uma mensalidade.
 * Existem tamb�m regras diferentes para caminh�es, que dependem do n�mero de
 * eixos e do valor da carga carregada, e para ve�culos para muitos passageiros,
 * como �nibus e vans.
 * 
 * @author andre
 *
 */
public class ContaEstacionamento {

	private CalculoValor calculo;

	private Veiculo veiculo;
	private long inicio, fim;

	public Double valorConta() {
		return calculo.calcular(fim - inicio, veiculo);
	}

	public void setCalculo(CalculoValor calculo) {
		this.calculo = calculo;
	}
}
