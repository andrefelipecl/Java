import java.util.Objects;

public class Main {

	public static void main(String[] args) {
		
		Carro c1 = new Carro();
		c1.setNome("Corcel");
		c1.setPotencia(2);
		c1.setVelocidade(0);
		
		Carro c3 = new Carro();
		c3.setNome("Corcel");
		c3.setPotencia(2);
		c3.setVelocidade(0);
		
		System.out.println();
		
		c1.acelerar();
		
		c1.imprimir();
		c3.imprimir();

		System.out.println();
		
		c3.acelerar();
		
		c1.imprimir();
		c3.imprimir();
	}
}
