import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Carro {

	int potencia;
	int velocidade;
	String nome;

	void acelerar() {
		velocidade += potencia;
	}

	void frear() {
		velocidade /= 2;
	}

	void imprimir() {
		System.out.println("O carro " + getNome() + " está a velocidade de: " + getVelocidade() + " km/h");
	}
}
